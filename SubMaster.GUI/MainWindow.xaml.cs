﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace SubMaster.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private Brush _originalBrush;
        public MainWindow()
        {
            InitializeComponent();
            _originalBrush = Background;
            StateToggleButton.IsChecked = true;
            StateToggleButton.Checked += StateToggleButton_Checked;
            StateToggleButton.Unchecked += StateToggleButton_Unchecked;
            Width = SystemParameters.PrimaryScreenWidth;
            this.AllowsTransparency = true;
            Background = new SolidColorBrush() { Opacity = 0 };


        }

        private void StateToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            var animation = new DoubleAnimation
            {
                To = 0.05,
                BeginTime = TimeSpan.FromSeconds(0),
                Duration = TimeSpan.FromSeconds(2),
                FillBehavior = FillBehavior.Stop
            };

            animation.Completed += (s, a) => StateToggleButton.Opacity = 0.05;

            StateToggleButton.BeginAnimation(OpacityProperty, animation);
            StateToggleButton.Content = "Show";
        }

        private void StateToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            StateToggleButton.Content = "Hide";

        }
        private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void MainWindow_OnDeactivated(object sender, EventArgs e)
        {

            Window window = (Window)sender;
            window.Topmost = true;
        }

        private void StateToggleButton_OnMouseEnter(object sender, MouseEventArgs e)
        {
            StateToggleButton.Opacity = 1;
        }

        private void StateToggleButton_OnMouseLeave(object sender, MouseEventArgs e)
        {
            if (StateToggleButton.IsChecked != null && !StateToggleButton.IsChecked.Value)
            {
                var animation = new DoubleAnimation
                {
                    To = 0.05,
                    BeginTime = TimeSpan.FromSeconds(0),
                    Duration = TimeSpan.FromSeconds(2),
                    FillBehavior = FillBehavior.Stop
                };

                animation.Completed += (s, a) => StateToggleButton.Opacity = 0.05;

                StateToggleButton.BeginAnimation(OpacityProperty, animation);
                StateToggleButton.Content = "Show";
            }
           
        }
    }
}
