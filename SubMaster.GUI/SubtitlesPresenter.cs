﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Prism.Commands;

namespace SubMaster.GUI
{
    public class SubtitlesPresenter : Notifier
    {
        private SubtitleItem _lastSubtitleItem;
        private bool _isRunning;
        private static object lockObject = new object();
        private SrtParser _parser;
        private MultimediaTimer _timer;
        private string _currentLine1;
        private string _currentLine2;
        private List<SubtitleItem> _subtitleItems;
        private string _currentSubtitlePath;
        private Encoding _encoding;
        private int _sliderMiniSeconds;
        private int _maximumSeconds;
        private int _deleyMs;

        public DelegateCommand PlayPauseCommand { get; set; }
        public DelegateCommand OnSliderValueChandedCommand { get; set; }
        public DelegateCommand OpenSubtitlePathDialogCommand { get; set; }

        public int DeleyMs
        {
            get { return _deleyMs; }
            set
            {
                _deleyMs = value;
                OnPropertyChanged("DeleyMs");
            }
        }
        public long MaximumSeconds => SubtitleItems.Last().EndTime;

        public int SliderMiniSeconds
        {
            get { return _sliderMiniSeconds; }
            set
            {
                _sliderMiniSeconds = value;
                OnPropertyChanged("SliderMiniSeconds");
            }
        }

        public string CurrentSubtitlePath
        {
            get { return _currentSubtitlePath; }
            set
            {
                _currentSubtitlePath = value;
                ResetStopWatch();
                OnPropertyChanged("CurrentSubtitlePath");
            }
        }
        public List<SubtitleItem> SubtitleItems
        {
            get { return _subtitleItems; }
            set
            {
                _subtitleItems = value;
                OnPropertyChanged("SubtitleItmes");
            }
        }
        public string CurrentLine1
        {
            get { return _currentLine1; }
            set
            {
                _currentLine1 = value;
                OnPropertyChanged("CurrentLine1");
            }
        }
        public string CurrentLine2
        {
            get { return _currentLine2; }
            set
            {
                _currentLine2 = value;
                OnPropertyChanged("CurrentLine2");
            }
        }
        public Encoding Encoding
        {
            get { return _encoding; }
            set
            {
                _encoding = value;
                OnPropertyChanged("Encoding");
            }
        }

        public SubtitlesPresenter()
        {

            _timer = new MultimediaTimer();
            _timer.Period = 10;
            _timer.Tick += UpdateLines;
            _isRunning = true;

            Encoding = Encoding.Default;
            OpenSubtitlePathDialogCommand = new DelegateCommand(OpenFileDialog);
            PlayPauseCommand = new DelegateCommand(PlayOrPause);
            DeleyMs = 0;
            _parser = new SrtParser();
            CurrentLine1 = "";
            CurrentLine2 = "";
            CurrentSubtitlePath = "Subs.srt";

        }


        private void PlayOrPause()
        {
            if (_isRunning)
            {
                _timer.Stop();
                _isRunning = false;
            }
            else
            {
                _timer.Start();
                _isRunning = true;
            }
        }

        private void OpenFileDialog()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();


            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".srt";
            dlg.Filter = "SRT Files (*.srt)|*.srt";


            // Display OpenFileDialog by calling ShowDialog method 
            bool? result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                CurrentSubtitlePath = filename;

            }
        }
        private void UpdateLines(object sender, EventArgs e)
        {
            lock (lockObject)
            {

                var realMsToGet = SliderMiniSeconds + DeleyMs;
                var currentSubtitleItem =
                    SubtitleItems.FirstOrDefault(x => x.StartTime < realMsToGet && x.EndTime > realMsToGet);
                if (_lastSubtitleItem != currentSubtitleItem)
                {
                    _lastSubtitleItem = currentSubtitleItem;
                    if (currentSubtitleItem != null)
                    {
                        if (currentSubtitleItem.Lines.Any())
                        {
                            CurrentLine1 = currentSubtitleItem.Lines[0];
                            if (currentSubtitleItem.Lines.Count > 1)
                            {
                                CurrentLine2 = currentSubtitleItem.Lines[1];
                            }
                            else
                            {
                                CurrentLine2 = "";
                            }
                        }
                        else
                        {
                            CurrentLine1 = "";
                            CurrentLine2 = "";
                        }
                    }
                    else
                    {
                        CurrentLine1 = "";
                        CurrentLine2 = "";
                    }
                }
                SliderMiniSeconds += (int)_timer.Period;

            }
        }

        private void ResetStopWatch()
        {
            ParseSubtitiles();
            OnPropertyChanged("MaximumSeconds");
            _timer.Stop();
            _timer.Start();
        }

        private void ParseSubtitiles()
        {
            using (var stream = new FileStream(CurrentSubtitlePath, FileMode.Open, FileAccess.ReadWrite))
            {
                SubtitleItems = _parser.ParseStream(stream, Encoding);
            }
        }
    }
}
